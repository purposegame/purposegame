import VictoryGesture from './Victory';
import ThumbsUpGesture from './ThumbsUp';
import GripGesture from './Grip';
export {
  VictoryGesture,
  ThumbsUpGesture,
  GripGesture

}
